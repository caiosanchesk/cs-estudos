﻿using System;
using op;

class Programa {
    static void Main(string[] args){    
        Console.Write("Digite o primeiro número: ");
        int x = Convert.ToInt32(Console.ReadLine());
        Console.Write("Digite o segundo número: ");
        int y = Convert.ToInt32(Console.ReadLine());

        Operacoes op = new(x, y);
    bool cont = true;
    while (cont){
        Console.WriteLine("Seus números são: "+op.X+" e "+op.Y);
        Console.Write("Digite a opção: \n[1] Somar\n[2] Subtrair\n[3] Multiplicar\n[4] Dividir\n[5] Novos números\n[6] SAIR\n  : ");
        int opcao = Convert.ToInt32(Console.ReadLine());
        switch(opcao){
            case 1:
                int soma = op.Soma();
                Console.WriteLine(op.X+" + "+op.Y+ " = "+soma);
                break;
            case 2:
                int sub = op.Sub();
                Console.WriteLine(op.X+" - "+op.Y+ " = "+sub);
                break;
            case 3:
                int mult = op.Mult();
                Console.WriteLine(op.X+" x "+op.Y+ " = "+mult);
                break;
            case 4: 
                double div = op.Div();
                Console.WriteLine(op.X+" / "+op.Y+ " = "+div.ToString("0.00"));
                break;
            case 5:
                Console.Write("Digite o primeiro número: ");
                op.X = Convert.ToInt32(Console.ReadLine());
                Console.Write("Digite o segundo número: ");
                op.Y = Convert.ToInt32(Console.ReadLine());
                break;
            case 6:
                Console.WriteLine("SAINDO DO PROGRAMA");
                cont = false;
                break;


        } 
    }
    }
}


