namespace op;
class Operacoes(int x, int y)
{

    public int X
    { get; set; } = x;
    public int Y
    { get; set; } = y;

    public int Soma(){
        return X + Y;
    }
    public int Sub(){
        return X - Y;
    }
    public int Mult(){
        return X * Y;
    }
    public double Div(){
        if(Y == 0){
            Console.WriteLine("Não é possível dividir por 0!");
            return 0;
        }
        return Convert.ToDouble(X) / Convert.ToDouble(Y);
    }

}