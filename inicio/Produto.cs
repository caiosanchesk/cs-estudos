namespace classe
{
class Produto {

    public string Nome
    {get; set;}
    public double Preco
    {get; set;}

    public Produto(string nome, double preco){
        
        Console.WriteLine("Estou sendo criado!");
        Nome = nome;
        Preco = preco;
    }

    ~Produto(){
        Console.WriteLine("Estou sendo morto!");
    }

    public string FormataPreco(){   
        return "R$ " + Preco.ToString("0.00");
    }

}
}