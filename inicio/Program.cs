﻿using System;
using classe;

class Hello{
    static void Main(string[] args){
        Console.WriteLine("Digite o nome do produto: ");
        string? nome = Console.ReadLine();
        Console.WriteLine("Digite o preço do produto: ");
        double preco = Convert.ToDouble(Console.ReadLine());
        
        Produto produto = new(nome!, preco);

        Console.WriteLine("Olá, "+produto.Nome+" vejo que vale "+produto.FormataPreco()+"!");

    }
}